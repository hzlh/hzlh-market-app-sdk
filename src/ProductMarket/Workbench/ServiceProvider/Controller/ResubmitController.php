<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\ResubmitControllerTrait;
use Common\Controller\Interfaces\IResubmitAbleController;

use Workbench\ServiceProvider\View\Template\ResubmitView;
use Workbench\ServiceProvider\Command\ServiceProvider\ResubmitServiceProviderCommand;
use Workbench\ServiceProvider\CommandHandler\ServiceProvider\ServiceProviderCommandHandlerFactory;

use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;

class ResubmitController extends Controller implements IResubmitAbleController
{
    use WebTrait, ResubmitControllerTrait, GlobalCheckTrait, OperationValidateTrait;

    private $commandBus;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceProviderCommandHandlerFactory());
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
        unset($this->repository);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    /**
     * @param $id
     * @return bool
     * @param [GET, POST]
     * @method /serviceProviders/id/resubmit
     * 重新认证
     */
    protected function resubmitView(int $id) : bool
    {
        $authentication = $this->getAuthenticationRepository()
            ->scenario(AuthenticationRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($authentication instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $this->render(new ResubmitView($authentication));
        return true;
    }

    protected function resubmitAction(int $id)
    {
        $image = $this->getRequest()->post('image', array());

        if ($this->validateResubmitScenario($image)) {
            $image = $this->checkImage($image);
            $command = new ResubmitServiceProviderCommand($image, $id);

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}

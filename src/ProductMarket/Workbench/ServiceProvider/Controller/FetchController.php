<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\Controller;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\GlobalCheckTrait;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use AppMarket\ProductMarket\Workbench\ServiceCategory\Controller\CategoryTrait;
use AppMarket\ProductMarket\Workbench\ServiceProvider\View\Template\AuditView;
use AppMarket\ProductMarket\Workbench\ServiceProvider\View\Template\ProcessView;
use AppMarket\ProductMarket\Workbench\ServiceProvider\View\Json\JsonDetailView;
use AppMarket\ProductMarket\Workbench\ServiceProvider\View\Json\JsonAuditView;

use Sdk\Common\Model\IApplyAble;
use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;

use Statistical\Controller\StatisticalControllerTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait, StatisticalControllerTrait, CategoryTrait;

    const TYPE = 'staticsServiceAuthenticationCount';

    const SERVICE_SIZE = 10;

    const SCENE = array(
        'AUDIT' => 1,
        'PASS' => 2,
        'REJECT' => 3
    );

    private $repository;

    private $concurrentAdapter;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthenticationRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->concurrentAdapter);
    }

    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }

    /**
     *
     */
    protected function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        if (!$this->globalCheckEnterprise()) {
            return false;
        }

        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());

        $repository = $this->getAuthenticationRepository();

        list($filter, $sort) = $this->serviceProviderFilterFormat($enterpriseId);
        list($page, $size) = $this->getPageAndSize(self::SERVICE_SIZE);

        $this->getConcurrentAdapter()->addPromise(
            'authentications',
            $repository->scenario(AuthenticationRepository::LIST_MODEL_UN)
                ->searchAsync(['enterprise' => $enterpriseId], $sort, $page, $size),
            $repository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'authentication',
            $repository->scenario(AuthenticationRepository::LIST_MODEL_UN)
                ->searchAsync($filter, $sort, $page, $size),
            $repository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        if (empty($data['authentications'])) {
            $this->process();
            return true;
        }
        //统计成为服务商不同状态的数量
        $serviceCount  = $this->getStaticsServiceRequirementCount($enterpriseId);

        if ($serviceCount instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonAuditView($data['authentication'], $serviceCount, self::TYPE));
            return true;
        }

        $this->render(new AuditView($data['authentication'], $serviceCount, self::TYPE));
        return true;
    }

    protected function serviceProviderFilterFormat($enterpriseId)
    {
        $scene = $this->getRequest()->get('scene', '');
        $scene = marmot_decode($scene);

        $sort = ['-updateTime'];

        $filter = array();
        $filter['enterprise'] = $enterpriseId;
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'];

        if ($scene !== '') {
            if ($scene == self::SCENE['PASS']) {
                $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
            }
            if ($scene == self::SCENE['REJECT']) {
                $filter['applyStatus'] = IApplyAble::APPLY_STATUS['REJECT'];
            }
        }

        return [$filter, $sort];
    }

    protected function getStaticsServiceRequirementCount($enterpriseId)
    {
        $filter['enterpriseId'] = $enterpriseId;

        $staticsServiceRequirementCount = $this->analyse(self::TYPE, $filter);

        return $staticsServiceRequirementCount;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /serviceProviders/id
     * 认证服务商详情
     */
    protected function fetchOneAction($id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        $authentication = $this->getAuthenticationRepository()
            ->scenario(AuthenticationRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($authentication instanceof INull) {
            return false;
        }

        $this->render(new JsonDetailView($authentication));
        return true;
    }

    /**
     * @return bool
     * @param [GET]
     * @method /serviceProviders/process
     * 服务商入驻流程
     */
    public function process()
    {
        $serviceCategory = $this->fetchServiceCategory();

        $this->render(new ProcessView($serviceCategory));
        return true;
    }
}

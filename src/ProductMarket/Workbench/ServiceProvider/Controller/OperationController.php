<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Interfaces\IOperatAbleController;

use AppMarket\ProductMarket\Workbench\ServiceCategory\Controller\CategoryTrait;
use AppMarket\ProductMarket\Workbench\ServiceProvider\View\Template\AddView;
use AppMarket\ProductMarket\Workbench\ServiceProvider\Command\ServiceProvider\AddServiceProviderCommand;
use AppMarket\ProductMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider\ServiceProviderCommandHandlerFactory;

use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, GlobalCheckTrait, OperationValidateTrait, CategoryTrait;

    private $commandBus;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceProviderCommandHandlerFactory());
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
        unset($this->repository);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    /**      */
    protected function addView() : bool
    {
        if (!$this->globalCheckEnterprise()) {
            $this->displayError();
            return false;
        }

        $serviceCategory = $this->fetchServiceCategory();
      
        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());
        
        list($filter, $sort) = $this->serverFilterFormatChange($enterpriseId);

        $authentication = array();
        list($count, $authentication) = $this->getRepository()
            ->scenario(AuthenticationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
 
        unset($count);

        $this->render(new AddView($serviceCategory, $authentication));
        return true;
    }

    protected function serverFilterFormatChange($enterpriseId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['enterprise'] = $enterpriseId;

        return [$filter, $sort];
    }

    /**      */
    protected function addAction()
    {
        $qualifications = $this->getRequest()->post('qualifications', array());
        foreach ($qualifications as $key => $qualification) {
            $qualifications[$key]['serviceCategoryId'] = marmot_decode($qualification['serviceCategoryId']);
            // if (isset($qualification['image'])) {
            //     $qualifications[$key]['image'] = $this->checkImage($qualification['image']);
            // }
        }

        $enterpriseId = Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId());
        
        if ($this->validateOperationScenario(
            $qualifications,
            $enterpriseId
        ) ) {
            $command = new AddServiceProviderCommand(
                $qualifications,
                $enterpriseId
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        return false;
    }

    protected function editAction(int $id) : bool
    {
        unset($id);
        return false;
    }
}

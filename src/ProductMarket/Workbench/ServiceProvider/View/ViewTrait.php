<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\View;

use AppMarket\ProductMarket\Workbench\ServiceProvider\Translator\ServiceProviderTranslator;

trait ViewTrait
{
    private $serviceCategories;

    private $authentication;

    private $serviceProviderTranslator;

    public function __construct($serviceCategories, $authentication = array())
    {
        parent::__construct();
        $this->serviceCategories = $serviceCategories;
        $this->authentication = $authentication;
        $this->serviceProviderTranslator = new ServiceProviderTranslator();
    }

    public function __destruct()
    {
        unset($this->serviceCategories);
        unset($this->authentication);
        unset($this->serviceProviderTranslator);
    }

    public function getServiceCategories() : array
    {
        return $this->serviceCategories;
    }

    public function getAuthentication()
    {
        return $this->authentication;
    }

    public function getServiceProviderTranslator() : ServiceProviderTranslator
    {
        return $this->serviceProviderTranslator;
    }

    public function getAuthenticationIds()
    {
        $authentications = $this->getAuthentication();

        $authenticationIds = array();
        if (!empty($authentications)) {
            $authenticationIds = $this->fetchAuthenticationIds($authentications);
        }

        return $authenticationIds;
    }

    protected function fetchAuthenticationIds($authentications)
    {
        $translator = $this->getServiceProviderTranslator();

        $serviceCategories = array();
        foreach ($authentications as $authentication) {
            $serviceCategories[] = $translator->objectToArray(
                $authentication,
                array('serviceCategory'=>[])
            );
        }

        $serviceCategoryIds = array();
        foreach ($serviceCategories as $serviceCategory) {
            $serviceCategoryIds[] = $serviceCategory['serviceCategory']['id'];
        }

        return $serviceCategoryIds;
    }
}

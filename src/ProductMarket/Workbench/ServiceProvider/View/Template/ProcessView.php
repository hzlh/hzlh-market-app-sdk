<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Workbench\ServiceProvider\View\ViewTrait;
use Workbench\ServiceCategory\View\ServiceCategoryViewTrait;

class ProcessView extends TemplateView implements IView
{
    use ViewTrait, ServiceCategoryViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $serviceCategories = $this->getServiceCategories();
        $serviceCategories = $this->getCategoryList($serviceCategories);
        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Service/Workbench/ServiceProvider/Process.tpl',
            [
                'data' => $serviceCategories,
                'permission' => $permission,
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_IDENTITY']
            ]
        );
    }
}

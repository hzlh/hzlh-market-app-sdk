<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\View\Template;

use Common\View\NavTrait;
use Common\Controller\Traits\GlobalCheckRolesTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Statistical\View\StatisticalViewTrait;

use AppMarket\ProductMarket\Workbench\ServiceProvider\View\AuditViewTrait;

class AuditView extends TemplateView implements IView
{
    use AuditViewTrait, StatisticalViewTrait, GlobalCheckRolesTrait;

    public function display()
    {
        $auditList = $this->getAuditList();

        $serviceAuthentication = $this->statisticalArray(
            $this->getType(),
            $this->getServiceAuthenticationCount()
        );

        $permission = $this->workbenchesRoles();

        $this->getView()->display(
            'ProductMarket/Service/Workbench/ServiceProvider/Audit.tpl',
            [
                'nav_left' => NavTrait::NAV_WORKBENCH['AUTHENTICATION'],
                'nav_phoneItem' => NavTrait::NAV_PHONE['WORKBENCH_SUPERMARKET'],
                'nav_phone' => NavTrait::NAV_PHONE['WORKBENCH_AUTHENTICATION'],
                'list' => $auditList,
                'total' => $this->getCount(),
                'permission' => $permission,
                'serviceAuthenticationCount' => $serviceAuthentication[0]
            ]
        );
    }
}

<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceProviderCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'AppMarket\ProductMarket\Workbench\ServiceProvider\Command\ServiceProvider\AddServiceProviderCommand'=>
        'AppMarket\ProductMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider\AddServiceProviderCommandHandler',
        'AppMarket\ProductMarket\Workbench\ServiceProvider\Command\ServiceProvider\ResubmitServiceProviderCommand'=>
        'AppMarket\ProductMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider\ResubmitServiceProviderCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

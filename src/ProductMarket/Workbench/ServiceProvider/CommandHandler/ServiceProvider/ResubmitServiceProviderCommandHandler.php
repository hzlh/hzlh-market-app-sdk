<?php
namespace AppMarket\ProductMarket\Workbench\ServiceProvider\CommandHandler\ServiceProvider;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ProductMarket\Authentication\Model\Authentication;
use Sdk\ProductMarket\Authentication\Model\NullAuthentication;
use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;

use AppMarket\ProductMarket\Workbench\ServiceProvider\Command\ServiceProvider\ResubmitServiceProviderCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class ResubmitServiceProviderCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    private $authentication;

    private $repository;

    public function __construct()
    {
        $this->authentication = new NullAuthentication();
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->authentication);
        unset($this->repository);
    }

    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function fetchAuthentication($id) : Authentication
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof ResubmitServiceProviderCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->authentication = $this->fetchAuthentication($command->id);

        $this->authentication->setQualificationImage($command->qualificationImage);

        if ($this->authentication->resubmit()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_RESUBMIT'],
            ILogAble::CATEGORY['SERVICE_PROVIDER'],
            $this->authentication->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->authentication->getNumber(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}

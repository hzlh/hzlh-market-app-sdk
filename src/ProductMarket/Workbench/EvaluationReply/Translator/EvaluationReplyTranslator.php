<?php
namespace AppMarket\ProductMarket\Workbench\EvaluationReply\Translator;

use Sdk\ProductMarket\EvaluationReply\Model\EvaluationReply;
use Sdk\ProductMarket\EvaluationReply\Model\NullEvaluationReply;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use UserCenter\Enterprise\Translator\EnterpriseTranslator;
use AppMarket\ProductMarket\UserCenter\Evaluation\Translator\EvaluationTranslator;

class EvaluationReplyTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $evaluation = null)
    {
        unset($evaluation);
        unset($expression);
        return NullEvaluationReply::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function getEnterpriseTranslator()
    {
        return new EnterpriseTranslator();
    }

    public function getEvaluationTranslator()
    {
        return new EvaluationTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function objectToArray($evaluation, array $keys = array())
    {
        if (!$evaluation instanceof EvaluationReply) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'content',
                'reason',
                'replyType',
                'fromReplyId',
                'evaluation' => [],
                'enterprise'=> [],
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($evaluation->getId());
        }
        if (in_array('content', $keys)) {
            $expression['content'] = Filter::dhtmlspecialchars(str_replace("ℑ", "", $evaluation->getContent()));
            //phpcs:ignore;
        }
        if (in_array('replyType', $keys)) {
            $expression['replyType'] = $evaluation->getReplyType();
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $evaluation->getReason();
        }
        if (in_array('fromReplyId', $keys)) {
            $expression['fromReplyId'] = $evaluation->getFromReplyId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $evaluation->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $evaluation->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $evaluation->getCreateTime());
            $expression['createTimeFullFormat'] = date('Y-m-d H:i:s', $evaluation->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $evaluation->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $evaluation->getUpdateTime());
        }
        if (isset($keys['evaluation'])) {
            $expression['evaluation'] = $this->getEvaluationTranslator()->objectToArray(
                $evaluation->getEvaluation(),
                $keys['evaluation']
            );
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $evaluation->getEnterprise(),
                $keys['enterprise']
            );
        }

        return $expression;
    }
}

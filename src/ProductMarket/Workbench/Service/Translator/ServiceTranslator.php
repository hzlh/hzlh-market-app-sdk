<?php
namespace AppMarket\ProductMarket\Workbench\Service\Translator;

use Marmot\Framework\Classes\Filter;
use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Service\Model\Service;
use Sdk\ProductMarket\Service\Model\NullService;

use Snapshot\Translator\SnapshotTranslator;

use Portal\Policy\Translator\PolicyCategoryTranslator;

use UserCenter\Enterprise\Translator\EnterpriseTranslator;

use AppMarket\ProductMarket\Workbench\ServiceCategory\Translator\ServiceCategoryTranslator;

class ServiceTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $service = null)
    {
        unset($expression);
        unset($service);
        return NullService::getInstance();
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getPolicyCategoryTranslator() : PolicyCategoryTranslator
    {
        return new PolicyCategoryTranslator();
    }

    protected function getSnapshotsTranslator() : SnapshotTranslator
    {
        return new SnapshotTranslator();
    }

    /**
     * 屏蔽类中所有PMD警告
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($service, array $keys = array())
    {
        if (!$service instanceof Service) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'title',
                'cover',
                'detail',
                'price',
                'minPrice',
                'maxPrice',
                'contract',
                'volume',
                'attentionDegree',
                'pageViews',
                'rejectReason',
                'applyStatus',
                'status',
                'createTime',
                'updateTime',
                'serviceObjects'=>[],
                'serviceCategory'=>[],
                'enterprise'=>[],
                'snapshots'=>[],
                'tag'
            );
        }
        $expression = array();
        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($service->getId());
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $service->getNumber();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $service->getTitle();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $service->getCover();
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = Filter::dhtmlspecialchars($service->getDetail());
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $service->getPrice();
            foreach ($expression['price'] as $key => $val) {
                $expression['price'][$key]['value'] = number_format($val['value'], 2, '.', '');
            }
        }
        if (in_array('minPrice', $keys)) {
            $expression['minPrice'] = $service->getMinPrice();
            $expression['minPrice'] = number_format($expression['minPrice'], 2, '.', '');
        }
        if (in_array('maxPrice', $keys)) {
            $expression['maxPrice'] = $service->getMaxPrice();
            $expression['maxPrice'] = number_format($expression['maxPrice'], 2, '.', '');
        }
        if (in_array('contract', $keys)) {
            $expression['contract'] = $service->getContract();
        }
        if (in_array('attentionDegree', $keys)) {
            $expression['attentionDegree'] = $service->getAttentionDegree();
        }
        if (in_array('volume', $keys)) {
            $expression['volume'] = $service->getVolume();
        }
        if (in_array('pageViews', $keys)) {
            $expression['pageViews'] = $service->getPageViews();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $service->getApplyStatus();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $service->getStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $service->getRejectReason();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $service->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $expression['createTime']);
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $service->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $expression['updateTime']);
        }
        if (isset($keys['serviceObjects'])) {
            $expression['serviceObjects'] = $this->setServiceObjectsArray($service->getServiceObjects(), array());
        }
        if (isset($keys['serviceCategory'])) {
            $expression['serviceCategory'] = $this->getServiceCategoryTranslator()->objectToArray(
                $service->getServiceCategory(),
                $keys['serviceCategory']
            );
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $service->getEnterprise(),
                $keys['enterprise']
            );
        }
        if (isset($keys['snapshots'])) {
            foreach ($service->getSnapshots() as $snapshot) {
                $expression['snapshots'] = $this->getSnapshotsTranslator()->objectToArray($snapshot);
            }
        }
        if (in_array('tag', $keys)) {
            $expression['tag'] = $service->getTag();
        }
        
        return $expression;
    }

    protected function setServiceObjectsArray($serviceObjects, $key)
    {
        $serviceObjectsArray = array();
        foreach ($serviceObjects as $serviceObject) {
            $serviceObjectsArray[] = $this->getPolicyCategoryTranslator()->objectToArray(
                $serviceObject,
                $key
            );
        }

        return $serviceObjectsArray;
    }
}

<?php
namespace AppMarket\ProductMarket\Workbench\Service\Controller;

use Marmot\Core;
use Sdk\Common\Model\IApplyAble;
use Sdk\ProductMarket\Policy\Model\PolicyModelFactory;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;
use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;

trait ServiceTrait
{
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return new AuthenticationRepository();
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return new ServiceRepository();
    }

    protected function fetchServiceCategoryByIds($ids)
    {
        $serviceCategory = array();
        list($count, $serviceCategory) = $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::LIST_MODEL_UN)->fetchList($ids);
            
        unset($count);

        return $serviceCategory;
    }

    protected function fetchServiceObjects() : array
    {
        $applicableObjects = array();

        foreach (PolicyModelFactory::POLICY_APPLICABLE_OBJECT as $key => $applicableObject) {
            $applicableObjects[] = array(
                'id' => $key,
                'name' => $applicableObject
            );
        }

        return $applicableObjects;
    }

    /**
     *
     */
    protected function getServiceCategoryIds($enterpriseId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['enterprise'] = $enterpriseId;
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
       
        $authentications = array();
        list($count, $authentications) = $this->getAuthenticationRepository()
            ->scenario(AuthenticationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
  
        unset($count);

        $serviceCategoryIds = array();
        foreach ($authentications as $authentication) {
            $serviceCategoryIds[] = $authentication->getServiceCategory()->getId();
        }

        return $serviceCategoryIds;
    }

    protected function getCommonRequest()
    {
        $request = $this->getRequest();

        $requestData = array();
        $requestData['title'] = $request->post('title', '');
        $requestData['cover'] = $request->post('cover', array());
        $requestData['price'] = $request->post('price', array());
        $requestData['detail'] = $request->post('detail', array());
        $requestData['contract'] = $request->post('contract', array());
        $requestData['serviceObjects'] = $request->post('serviceObjects', array());
        $requestData['tag'] = $request->post('tags', '');
        $requestData['tag'] = $this->tagsImplode($requestData['tag']);

        $serviceCategoryId = $request->post('serviceCategory', '');
        $requestData['serviceCategoryId'] = marmot_decode($serviceCategoryId);
        $requestData['enterpriseId'] = Core::$cacheDriver->fetch(
            'staffEnterpriseId:'.Core::$container->get('user')->getId()
        );

        return $requestData;
    }

    protected function tagsImplode($tags) : string
    {
        $tags = explode(',', $tags);
        foreach ($tags as $val) {
            $tagData[] = marmot_decode($val);
        }

        return implode(',', $tagData);
    }

    protected function getServiceDetail($id)
    {
        $service = $this->getServiceRepository()
                      ->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
                      ->fetchOne($id);

        return $service;
    }

    protected function getCoverAndDetail($cover, $details) : array
    {
        if ($cover) {
            $cover = $this->checkImageNormal($cover);
        }

        $details = $this->checkDetailImage($details);

        return [$cover, $details];
    }
}

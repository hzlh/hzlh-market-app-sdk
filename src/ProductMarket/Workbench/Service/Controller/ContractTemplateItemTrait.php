<?php
namespace AppMarket\ProductMarket\Workbench\Service\Controller;

use Qxy\Contract\Template\Repository\TemplateItemRepository;
use Qxy\Contract\Template\Model\TemplateItem;

use Qxy\Contract\Statistical\Model\Statistical;

use Statistical\Controller\StatisticalControllerTrait;

trait ContractTemplateItemTrait
{
    use StatisticalControllerTrait;

    protected function getTemplateItemRepository()
    {
        return new TemplateItemRepository();
    }

    protected function filterContractTemplateItem($services) : array
    {
        $serviceId = '';

        if (is_array($services)) {
            $itemId = [];
            foreach ($services as $service) {
                $itemId[] = 'fw_'.$service->getId();
            }
            $serviceId = join(',', $itemId);
        }

        if (!is_array($services)) {
            $serviceId = 'fw_'.$services->getId();
        }

        $sort = ['-updateTime'];

        $filter['itemId'] = $serviceId;
        $filter['status'] = TemplateItem::STATUS['BIND'];

        $templateItemList = [];

        list($count, $templateItemList) = $this->getTemplateItemRepository()
            ->scenario(TemplateItemRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $templateItemList;
    }

    protected function getTemplateIds(array $templateItemList)
    {
        $templateIds = [];
        foreach ($templateItemList as $value) {
            $templateIds[] = intval($value->getTemplate()->getId());
        }

        return implode(',', $templateIds);
    }

    public function fetchPerformanceStatics($contractFilter, $type)
    {
        $filter = [];

        $staticsType = [
          'CONTRACT_PERFORMANCE_COUNT' => 'staticsContractPerformanceCount',
          'CONTRACT_USAGE_COUNT' =>'staticsContractUsageCount',
          'ENTERPRISE_CONTRACT_COUNT' =>'staticsEnterpriseContractCount'
        ];

        if ($type == $staticsType['CONTRACT_USAGE_COUNT']) {
            $filter['templateId'] = $contractFilter;
        }

        if ($type == $staticsType['CONTRACT_PERFORMANCE_COUNT']) {
            $filter['template'] = $contractFilter;
        }

        if ($type == $staticsType['ENTERPRISE_CONTRACT_COUNT']) {
            $filter['enterpriseId'] = $contractFilter;
        }

        $staticsList = $this->contractAnalyse($type, $filter);

        return ["data"=>$staticsList,"type"=>$type];
    }

    protected function getListIds(array $list)
    {
        $ids = [];
        $ids = implode(',', array_keys($list));

        return $ids;
    }

    private function getEnterpriseList($enterpriseIds)
    {
        list($count, $list) = $this->getEnterpriseRepository()
                                   ->fetchList($enterpriseIds);
        unset($count);

        return $list;
    }
}

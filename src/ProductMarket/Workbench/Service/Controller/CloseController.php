<?php
namespace AppMarket\ProductMarket\Workbench\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\CloseControllerTrait;
use Common\Controller\Interfaces\ICloseAbleController;

use AppMarket\ProductMarket\Workbench\Service\Command\Service\CloseServiceCommand;
use AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

class CloseController extends Controller implements ICloseAbleController
{
    use WebTrait, CloseControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function closeAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new CloseServiceCommand($id));
    }
}

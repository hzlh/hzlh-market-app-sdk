<?php
namespace AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\AddServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\AddServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\EditServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\EditServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\ResubmitServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\RevokeServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\DeleteServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\CloseServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\OnShelfServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'AppMarket\ProductMarket\Workbench\Service\Command\Service\OffStockServiceCommand'=>
        'AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

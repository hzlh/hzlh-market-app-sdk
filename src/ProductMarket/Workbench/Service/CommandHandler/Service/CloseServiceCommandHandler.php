<?php
namespace AppMarket\ProductMarket\Workbench\Service\CommandHandler\Service;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use AppMarket\ProductMarket\Workbench\Service\Command\Service\CloseServiceCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class CloseServiceCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof CloseServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->service = $this->fetchService($command->id);

        if ($this->service->close()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_CLOSE'],
            ILogAble::CATEGORY['SERVICE'],
            $this->service->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->service->getNumber(),
            Core::$cacheDriver->fetch('staffEnterpriseId:'.Core::$container->get('user')->getId())
        );
    }
}

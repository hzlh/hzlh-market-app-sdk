<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\Controller;

use WidgetRules\Common\WidgetRules;

trait OperationValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function validateOperationScenario(
        $content
    ) {
        return $this->getWidgetRules()->reason($content);
    }
}

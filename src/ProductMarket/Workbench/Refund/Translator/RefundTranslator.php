<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;
use Sdk\ProductMarket\Order\RefundOrder\Model\NullRefundOrder;

use UserCenter\Member\Translator\MemberTranslator;
use UserCenter\Enterprise\Translator\EnterpriseTranslator;
use AppMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

class RefundTranslator implements ITranslator
{
    protected function getMemberTranslator() : ITranslator
    {
        return new MemberTranslator();
    }

    protected function getEnterpriseTranslator() : ITranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getOrderTranslator() : ITranslator
    {
        return new OrderTranslator();
    }
    
    public function arrayToObject(array $expression, $refundOrder = null)
    {
        unset($refundOrder);
        unset($expression);
        return NullRefundOrder::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function objectToArray($refundOrder, array $keys = array())
    {
        if (!$refundOrder instanceof RefundOrder) {
            return array();
        }
        if (empty($keys)) {
            $keys = array(
                'id',
                'refundReason',
                'refuseReason',
                'voucher',
                'orderNumber',
                'orderCategory',
                'goodsName',
                'createTime',
                'updateTime',
                'timeRecord',
                'statusTime',
                'status',
                'order'=>[],
                'member'=>[],
                'enterprise'=>[],
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] =  marmot_encode($refundOrder->getId());
        }
        if (in_array('refundReason', $keys)) {
            $expression['refundReason'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $refundOrder->getRefundReason()));//phpcs:ignore;
        }
        if (in_array('refuseReason', $keys)) {
            $expression['refuseReason'] = Filter::dhtmlspecialchars(str_replace("ℑ", "&lt;br&gt;", $refundOrder->getRefuseReason()));//phpcs:ignore;
        }
        if (in_array('voucher', $keys)) {
            $expression['voucher'] = $refundOrder->getVoucher();
        }
        if (in_array('orderNumber', $keys)) {
            $expression['orderNumber'] = $refundOrder->getOrderNumber();
        }
        if (in_array('orderCategory', $keys)) {
            $expression['orderCategory'] = $refundOrder->getOrderCategory();
        }
        if (in_array('goodsName', $keys)) {
            $expression['goodsName'] = $refundOrder->getGoodsName();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $refundOrder->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $refundOrder->getCreateTime());
        }
        if (in_array('timeRecord', $keys)) {
            $timeRecord = $refundOrder->getTimeRecord();
            foreach ($timeRecord as &$time) {
                if (!empty($time)) {
                    $time = date('Y-m-d H:i:s', $time);
                }
            }
            $expression['timeRecord'] = $timeRecord;
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $refundOrder->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $refundOrder->getUpdateTime());
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $refundOrder->getStatusTime();
            $expression['statusTimeFormat'] = date('Y-m-d', $refundOrder->getStatusTime());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $refundOrder->getStatus();
            $expression['statusFormat'] = RefundOrder::REFUND_STATUS_BUYER_CN[$refundOrder->getStatus()];
        }
        if (isset($keys['order'])) {
            $expression['order'] = $this->getOrderTranslator()->objectToArray(
                $refundOrder->getOrder(),
                $keys['order']
            );
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $refundOrder->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $refundOrder->getEnterprise(),
                $keys['enterprise']
            );
        }

        return $expression;
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use AppMarket\ProductMarket\UserCenter\Refund\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $data = $this->getRefundList();

        $this->getView()->display(
            'ProductMarket/UserCenter/Refund/List.tpl',
            [
                'list' => $data,
                'total' => $this->getRefundTotal(),
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['REFUND']
            ]
        );
    }
}

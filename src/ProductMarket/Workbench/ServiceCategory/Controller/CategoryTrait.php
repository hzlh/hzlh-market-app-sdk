<?php
namespace AppMarket\ProductMarket\Workbench\ServiceCategory\Controller;

use Sdk\Common\Model\IEnableAble;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;

trait CategoryTrait
{
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    /**
     * @return array
     * 获取服务启用的二级服务分类
     *
     */
    protected function fetchServiceCategory()
    {
        list($filter, $sort) = $this->filterCategoryFormatChange();

        $serviceCategory = array();
        list($count, $serviceCategory) = $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        return $serviceCategory;
    }

    protected function filterCategoryFormatChange()
    {
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $sort = ['id'];

        return [$filter, $sort];
    }
}

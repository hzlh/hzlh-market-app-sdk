<?php
namespace AppMarket\ProductMarket\Service\Home\Cache\Query;

use Marmot\Framework\Query\FragmentCacheQuery;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use AppMarket\ProductMarket\Service\Home\Cache\Persistence\HomeCache;

use Sdk\ProductMarket\Service\Model\Service;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;
use Sdk\ProductMarket\ServiceRequirement\Repository\ServiceRequirementRepository;
use Sdk\Policy\Repository\PolicyRepository;
use Sdk\Enterprise\Repository\EnterpriseRepository;

use AppMarket\ProductMarket\Service\Home\Cache\ConcurrentIndexTrait;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\Model\IOnShelfAble;

class HomeFragmentCacheQuery extends FragmentCacheQuery
{
    use ConcurrentIndexTrait;

    const TTL = 60;
    const DEFAULT_PAGE = 1;
    const INDEX_SERVICE_SIZE = 6;
    const INDEX_REQUIREMENT_SIZE = 4;
    const INDEX_POLICY_SIZE = 4;
    const INDEX_ENTERPRISE_SIZE = 9;

    private $serviceRepository;

    private $requirementsRepository;

    private $policyRepository;

    private $enterpriseRepository;

    private $concurrentAdapter;

    public function __construct()
    {
        parent::__construct('marketHome', new HomeCache());
        $this->serviceRepository = new ServiceRepository();
        $this->requirementsRepository = new ServiceRequirementRepository();
        $this->policyRepository = new PolicyRepository();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->serviceRepository);
        unset($this->requirementsRepository);
        unset($this->policyRepository);
        unset($this->enterpriseRepository);
        unset($this->concurrentAdapter);
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->serviceRepository;
    }

    protected function getServiceRequirementRepository() : ServiceRequirementRepository
    {
        return $this->requirementsRepository;
    }

    protected function getPolicyRepository() : PolicyRepository
    {
        return $this->policyRepository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }

    protected function fetchCacheData()
    {
        $serviceRepository = $this->getServiceRepository()
                                  ->scenario(ServiceRepository::PORTAL_LIST_MODEL_UN);
        $requirementsRepository = $this->getServiceRequirementRepository()
                                       ->scenario(ServiceRequirementRepository::PORTAL_LIST_MODEL_UN);
        $policyRepository = $this->getPolicyRepository()
                                 ->scenario(PolicyRepository::PORTAL_LIST_MODEL_UN);
        $enterpriseRepository = $this->getEnterpriseRepository()
                                     ->scenario(EnterpriseRepository::PORTAL_LIST_MODEL_UN);

        list($serviceFilter, $requirementsFilter, $policyFilter, $enterpriseFilter, $sort)
            = $this->filterFormatChange();

        $this->getConcurrentAdapter()->addPromise(
            'service',
            $serviceRepository->searchAsync($serviceFilter, $sort, self::DEFAULT_PAGE, self::INDEX_SERVICE_SIZE),
            $serviceRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'requirements',
            $requirementsRepository
            ->searchAsync($requirementsFilter, $sort, self::DEFAULT_PAGE, self::INDEX_REQUIREMENT_SIZE),
            $requirementsRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'policy',
            $policyRepository->searchAsync($policyFilter, $sort, self::DEFAULT_PAGE, self::INDEX_POLICY_SIZE),
            $policyRepository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'enterprise',
            $enterpriseRepository
            ->searchAsync($enterpriseFilter, $sort, self::DEFAULT_PAGE, self::INDEX_ENTERPRISE_SIZE),
            $enterpriseRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        $data = $this->indexArray($data);

        return $data;
    }

    private function filterFormatChange()
    {
        $sort = ['-updateTime'];

        $serviceFilter['status'] = Service::SERVICE_STATUS['ONSHELF'];
        $serviceFilter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        $serviceFilter['parentCategory'] = 1;//一级分类为人力资源

        $requirementsFilter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        $requirementsFilter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        $policyFilter['status'] = IOnShelfAble::STATUS['ONSHELF'];

        $enterpriseFilter['status'] = 0;

        return [
            $serviceFilter,
            $requirementsFilter,
            $policyFilter,
            $enterpriseFilter,
            $sort
        ];
    }

     /**
     * 获取片段,如果缓存失效则必须重新更新该片段缓存
     */
    public function get()
    {
        //从缓存获取数据
        $cacheData = $this->getCacheLayer()->get($this->getFragmentKey());

        if ($cacheData) {
            return $cacheData;
        }

        $cacheData = $this->refresh();

        if (!$cacheData) {
            return array();
        }

        return $cacheData;
    }

    protected function getTtl() : int
    {
        return self::TTL;
    }
}

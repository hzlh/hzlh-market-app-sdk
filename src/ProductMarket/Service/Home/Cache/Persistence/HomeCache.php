<?php
namespace AppMarket\ProductMarket\Service\Home\Cache\Persistence;

use Marmot\Core;
use Marmot\Framework\Classes\Cache;

class HomeCache extends Cache
{
    public function __construct()
    {
        parent::__construct('marketHome'.Core::$container->get('user')->getId());
    }
}

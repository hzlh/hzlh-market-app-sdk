<?php
namespace AppMarket\ProductMarket\Service\Home\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use AppMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;
use AppMarket\ProductMarket\Workbench\ServiceCategory\Translator\ServiceCategoryTranslator;

class BannerIndexJsonView extends JsonView implements IView
{
    private $bannerServiceList;
    private $serviceTranslator;
    private $serviceCategoryList;
    private $serviceCategoryTranslator;

    public function __construct(
        array $serviceCategoryList,
        array $bannerServiceList
    ) {
        $this->bannerServiceList = $bannerServiceList;
        $this->serviceTranslator = new ServiceTranslator();
        $this->serviceCategoryList = $serviceCategoryList;
        $this->serviceCategoryTranslator = new ServiceCategoryTranslator();
        parent::__construct();
    }

    protected function getBannerServiceList() : array
    {
        return $this->bannerServiceList;
    }

    protected function getServiceCategoryList() : array
    {
        return $this->serviceCategoryList;
    }

    protected function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return $this->serviceCategoryTranslator;
    }

    public function display() : void
    {
        $data = array();

        $serviceCategories = $this->serviceCategoryRender();
        $bannerServices = $this->bannerServiceRender();

        $data = array(
          'serviceCategories' => $serviceCategories,
          'bannerServices' => $bannerServices
        );

        $this->encode($data);
    }

    private function serviceCategoryRender() : array
    {
        $translator = $this->getServiceCategoryTranslator();
        $serviceCategories = array();

        foreach ($this->getServiceCategoryList() as $serviceCategory) {
            $serviceCategories[] = $translator->objectToArray(
                $serviceCategory,
                array(
                  'id','name'
                )
            );
        }

        return $serviceCategories;
    }

    private function bannerServiceRender()
    {
        $translator = $this->getServiceTranslator();
        $bannerServices = array();

        foreach ($this->getBannerServiceList() as $service) {
            $bannerServices[] = $translator->objectToArray(
                $service,
                array(
                  'id',
                  'title',
                  'enterprise'=>['id', 'logo', 'name'],
                  'serviceCategory'=>['id', 'name'],
                  'minPrice',
                  'cover'
                )
            );
        }

        return $bannerServices;
    }
}

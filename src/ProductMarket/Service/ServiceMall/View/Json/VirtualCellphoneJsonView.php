<?php
namespace AppMarket\ProductMarket\Service\ServiceMall\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

class VirtualCellphoneJsonView extends JsonView implements IView
{
    private $data;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
    }

    public function __destruct()
    {
        unset($this->data);
    }

    protected function getData()
    {
        return $this->data;
    }

    public function display() : void
    {
        $data = $this->getData();

        $this->encode($data);
    }
}

<?php
namespace AppMarket\ProductMarket\Service\ServiceMall\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use AppMarket\ProductMarket\Service\ServiceMall\View\ViewTrait;

class ServiceEvaluationListView extends JsonView implements IView
{
    use ViewTrait;

    public function display() : void
    {
        $evaluationData =[];
        $serviceEvaluationData = $this->data();
        $evaluationData['evaluation'] = $serviceEvaluationData['evaluation'];
        $evaluationData['commodityScore'] = $serviceEvaluationData['commodityScore'];
        $count = $serviceEvaluationData['evaluationCount'];
        $enterpriseCollection = $this->getEnterpriseCollectionList();
        $enterpriseCollectionAll = $this->getEnterpriseCollectionAll();

        $collection = $this->getCollectionList();
        $collectionAll = $this->getCollectionAll();

        $data = array(
          'list' => $serviceEvaluationData['evaluation'],
          'commodityScore' => $serviceEvaluationData['commodityScore'],
          'enterpriseScore' => $serviceEvaluationData['enterpriseScore'],
          'total' => $count,
          'collection' => $collection,
          'collectionAll' => $collectionAll,
          'enterpriseCollection' => $enterpriseCollection,
          'enterpriseCollectionAll' => $enterpriseCollectionAll
        );

        $this->encode($data);
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\Order\Command\Order;

use Marmot\Interfaces\ICommand;

class AddOrderCommand implements ICommand
{
    public $remark;

    public $couponIds;

    public $commodity;

    public $address;

    public $memberAccountId;

    public function __construct(
        string $remark,
        array $couponIds,
        array $commodity,
        array $address,
        int $memberAccountId = 0
    ) {
        $this->remark = $remark;
        $this->couponIds = $couponIds;
        $this->commodity = $commodity;
        $this->address = $address;
        $this->memberAccountId = $memberAccountId;
    }
}

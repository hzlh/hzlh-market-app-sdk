<?php
namespace AppMarket\ProductMarket\UserCenter\Order\Command\Order;

use Marmot\Interfaces\ICommand;

class ConfirmationOrderCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}

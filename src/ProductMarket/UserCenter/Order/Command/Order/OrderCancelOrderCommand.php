<?php
namespace AppMarket\ProductMarket\UserCenter\Order\Command\Order;

use Marmot\Interfaces\ICommand;

class OrderCancelOrderCommand implements ICommand
{
    public $cancelReason;

    public $id;

    public function __construct(
        int $cancelReason = 0,
        int $id = 0
    ) {
        $this->cancelReason = $cancelReason;
        $this->id = $id;
    }
}

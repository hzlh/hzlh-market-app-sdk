<?php
namespace AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use AppMarket\ProductMarket\UserCenter\Order\Command\Order\PermanentDeleteOrderCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class PermanentDeleteOrderCommandHandler implements ICommandHandler, ILogAble
{
    use OrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof PermanentDeleteOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceOrder = $this->fetchOrder($command->id);

        if ($this->serviceOrder->buyerPermanentDelete()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_PERMANENT_DELETE'],
            ILogAble::CATEGORY['ORDER'],
            $this->serviceOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->serviceOrder->getOrderno()
        );
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class OrderCommandHandlerFactory implements ICommandHandlerFactory
{
    /**
     * 提交订单
     * 确认订单
     * 删除订单
     * 永久删除订单
     * 订单取消
     */
    const MAPS = array(
        'AppMarket\ProductMarket\UserCenter\Order\Command\Order\AddOrderCommand'=>
        'AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\AddOrderCommandHandler',
        'AppMarket\ProductMarket\UserCenter\Order\Command\Order\ConfirmationOrderCommand'=>
        'AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\ConfirmationOrderCommandHandler',
        'AppMarket\ProductMarket\UserCenter\Order\Command\Order\OrderCancelOrderCommand'=>
        'AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\OrderCancelOrderCommandHandler',
        'AppMarket\ProductMarket\UserCenter\Order\Command\Order\DeleteOrderCommand'=>
        'AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\DeleteOrderCommandHandler',
        'AppMarket\ProductMarket\UserCenter\Order\Command\Order\PermanentDeleteOrderCommand'=>
        'AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\PermanentDeleteOrderCommandHandler',
        'AppMarket\ProductMarket\UserCenter\Order\Command\Order\UpdateAddressOrderCommand'=>
        'AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order\UpdateAddressOrderCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

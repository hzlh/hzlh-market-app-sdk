<?php
namespace AppMarket\ProductMarket\UserCenter\Order\CommandHandler\Order;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use AppMarket\ProductMarket\UserCenter\Order\Command\Order\UpdateAddressOrderCommand;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class UpdateAddressOrderCommandHandler implements ICommandHandler, ILogAble
{
    use OrderCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof UpdateAddressOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceOrder = $this->fetchOrder($command->id);

        // 获取收货地址快照信息
        $addressSnapshot = $this->getSnapshot();
        $addressSnapshot->setId(marmot_decode($command->snapshotId));

        $orderAddress = $this->getOrderAddress();
        $orderAddress->setCellphone($command->cellphone);
        $orderAddress->setSnapshot($addressSnapshot);

        $this->serviceOrder->setOrderAddress($orderAddress);

        if ($this->serviceOrder->updateOrderAddress()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_EDIT'],
            ILogAble::CATEGORY['ORDER'],
            $this->serviceOrder->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->serviceOrder->getOrderno()
        );
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use AppMarket\ProductMarket\UserCenter\Order\View\DetailViewTrait;

class DetailView extends TemplateView implements IView
{
    use DetailViewTrait;

    public function display()
    {
        $data = $this->getOrder();

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/Detail.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['MY_ORDER'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_ORDER'],
                'data' => $data
            ]
        );
    }
}

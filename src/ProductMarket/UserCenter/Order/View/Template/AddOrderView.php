<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use AppMarket\ProductMarket\UserCenter\Order\View\AddViewTrait;
use AppMarket\ProductMarket\UserCenter\Order\Controller\OrderTrait;

class AddOrderView extends TemplateView implements IView
{
    use AddViewTrait, OrderTrait;

    const PROBABILITY = 100;

    const UPDATE_ORDER_AMOUNT_LOWER_LIMIT = 0.01;

    const MAX_COUPON_AMOUNT = 200;

    //获取符合要求的店铺优惠劵
    protected function getMerchantCouponFormat($merchantCoupon, $totalPrice, $service)
    {
        foreach ($merchantCoupon as $key => $item) {
            $coupon = array($item['id']);
            $merchantCoupon[$key]['subsidy'] = $this->businessPreferential($totalPrice, $coupon);
            if ($item['merchantCoupon']['reference']['id'] != $service['enterprise']['id']) {
                unset($merchantCoupon[$key]);
            }
            if ($item['merchantCoupon']['validityStartTime'] > time()) {
                unset($merchantCoupon[$key]);
            }
        }

        return array_values($merchantCoupon);
    }

    //获取符合要求的平台优惠劵
    protected function getPlatformCouponFormat($platformCoupon, $totalPrice)
    {
        foreach ($platformCoupon as $key => $item) {
            $coupon = array($item['id']);
            $platformCoupon[$key]['subsidy'] = $this->platformPreferential($totalPrice, $coupon);
            if ($item['merchantCoupon']['validityStartTime'] > time()) {
                unset($platformCoupon[$key]);
            }
        }

        return array_values($platformCoupon);
    }

    //根据优惠劵额度降序和过期时间升序排序
    protected function getSortArray($array)
    {
        foreach ($array as $key => $value) {
            $subsidy[$key] = $value['subsidy'];
            $validityEndTime[$key] = $value['merchantCoupon']['validityEndTime'];
        }

        array_multisort($subsidy, SORT_NUMERIC, SORT_DESC, $validityEndTime, SORT_NUMERIC, SORT_ASC, $array);

        return $array;
    }

    //获取优惠额度高且可叠加的优惠劵
    protected function getIsSuperposition($array)
    {
        foreach ($array as $key => $item) {
            if ($item['merchantCoupon']['isSuperposition'] == 0) {
                unset($array[$key]);
            }
        }

        return array_values($array);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * 获取平台和店铺额度最高的优惠劵
     */
    protected function getCouponWell($merchantCouponFormat, $platformCouponFormat)
    {
        //默认店铺优惠劵额度为0,店铺可叠加优惠劵额度为0
        $merchantCouponSubsidy = 0;
        $merchantIsSuperpositionSubsidy = 0;
        if (!empty($merchantCouponFormat)) {
            $merchantCouponSubsidy = $merchantCouponFormat[0]['subsidy'];
            $merchantIsSuperposition = $this->getIsSuperposition($merchantCouponFormat);

            if (!empty($merchantIsSuperposition)) {
                $merchantIsSuperpositionSubsidy = $merchantIsSuperposition[0]['subsidy'];
            }
        }

        //默认平台优惠劵额度为0,平台可叠加优惠劵额度为0
        $platformCouponSubsidy = 0;
        $platformIsSuperpositionSubsidy = 0;
        if (!empty($platformCouponFormat)) {
            $platformCouponSubsidy = $platformCouponFormat[0]['subsidy'];
            $platformIsSuperposition = $this->getIsSuperposition($platformCouponFormat);

            if (!empty($platformIsSuperposition)) {
                $platformIsSuperpositionSubsidy = $platformIsSuperposition[0]['subsidy'];
            }
        }

        //合计可叠加的店铺优惠劵和平台优惠劵额度
        $isSuperpositionTotal = $merchantIsSuperpositionSubsidy+$platformIsSuperpositionSubsidy;

        $couponWell = array();

        //平台和店铺都没有符合条件的优惠劵,返回空
        if ($platformCouponSubsidy == 0 && $merchantCouponSubsidy == 0) {
            return $couponWell;
        }

        //平台优惠劵优惠额度和店铺优惠劵优惠额度都小于平台和店铺组合的优惠额度
        if ($merchantCouponSubsidy < $isSuperpositionTotal && $platformCouponSubsidy < $isSuperpositionTotal) {
            $couponWell = array(
                'merchant' => $merchantIsSuperposition[0],
                'platform' => $platformIsSuperposition[0],
                'subsidy' => $merchantIsSuperposition[0]['subsidy']+$platformIsSuperposition[0]['subsidy'],
                'scene' => 1
            );

            return $couponWell;
        }

        //平台优惠劵优惠额度小于店铺优惠劵优惠额度的情况
        if ($platformCouponSubsidy < $merchantCouponSubsidy) {
            $couponWell = array(
                'merchant' => $merchantCouponFormat[0],
                'subsidy' => $merchantCouponFormat[0]['subsidy'],
                'scene' => 2
            );

            return $couponWell;
        }

        //平台优惠劵优惠额度大于店铺优惠劵优惠额度的情况
        if ($platformCouponSubsidy >= $merchantCouponSubsidy) {
            $couponWell = array(
                'platform' => $platformCouponFormat[0],
                'subsidy' => $platformCouponFormat[0]['subsidy'],
                'scene' => 3
            );

            return $couponWell;
        }

        return $couponWell;
    }

    public function display()
    {
        $data = $this->getList();
        $totalPrice = $data['totalPrice'];

        //获取符合条件的店铺优惠劵
        $merchantCoupon = $data['merchantCoupon'];
        $merchantCouponFormat = array();
        if (!empty($merchantCoupon)) {
            $merchantCouponFormat = $this->getMerchantCouponFormat($merchantCoupon, $totalPrice, $data['service']);
            if (!empty($merchantCouponFormat)) {
                $merchantCouponFormat = $this->getSortArray($merchantCouponFormat);
            }
        }

        //获取符合条件的平台优惠劵
        $platformCoupon = $data['platformCoupon'];
        $platformCouponFormat = array();
        if (!empty($platformCoupon)) {
            $platformCouponFormat = $this->getPlatformCouponFormat($platformCoupon, $totalPrice);
            if (!empty($platformCouponFormat)) {
                $platformCouponFormat = $this->getSortArray($platformCouponFormat);
            }
        }

        //获取优惠额度最高的优惠劵
        $couponWell = array();
        if (!empty($merchantCouponFormat) || !empty($platformCouponFormat)) {
            $couponWell = $this->getCouponWell($merchantCouponFormat, $platformCouponFormat);
            if (!empty($couponWell)) {
                $paidAmount = $totalPrice - $couponWell['subsidy'];
                $couponWell['paidAmount'] = $paidAmount <= 0 ? self::UPDATE_ORDER_AMOUNT_LOWER_LIMIT : $paidAmount;
            }
        }

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/AddOrder.tpl',
            [
                'merchantCoupon' => $merchantCouponFormat,
                'platformCoupon' => $platformCouponFormat,
                'order' => $data['order'],
                'service' => $data['service'],
                'templateItem' => $data['templateItem'],
                'totalPrice' => $totalPrice,
                'couponWell' => $couponWell,
                'realNameAuthenticationStatus' => $data['realNameAuthenticationStatus']
            ]
        );
    }
}

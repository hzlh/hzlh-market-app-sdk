<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use AppMarket\ProductMarket\UserCenter\Order\View\AddressViewTrait;

class AddressView extends TemplateView implements IView
{
    use AddressViewTrait;

    public function display()
    {
        $data = $this->getAddressList();

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/Address.tpl',
            [
                'list' => $data
            ]
        );
    }
}

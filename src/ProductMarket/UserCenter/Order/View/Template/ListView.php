<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use AppMarket\ProductMarket\UserCenter\Order\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getList();

        $this->getView()->display(
            'ProductMarket/UserCenter/Order/List.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['MY_ORDER'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_ORDER'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}

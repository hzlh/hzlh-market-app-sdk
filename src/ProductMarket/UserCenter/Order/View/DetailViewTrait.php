<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View;

use AppMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

trait DetailViewTrait
{
    private $data;

    private $translator;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
        $this->translator = new OrderTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->translator);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getTranslator() : OrderTranslator
    {
        return $this->translator;
    }

    protected function getOrder()
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray($this->getData());

        return $data;
    }
}

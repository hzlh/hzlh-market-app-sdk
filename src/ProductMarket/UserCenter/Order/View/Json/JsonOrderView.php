<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\ProductMarket\ServiceOrder\Model\ServiceOrder;
use AppMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

class JsonOrderView extends JsonView implements IView
{
    private $serviceOrder;

    private $translator;

    public function __construct(ServiceOrder $serviceOrder)
    {
        parent::__construct();
        $this->serviceOrder = $serviceOrder;
        $this->translator = new OrderTranslator();
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getTranslator() : OrderTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray(
            $this->getServiceOrder()
        );

        $this->encode($data);
    }
}

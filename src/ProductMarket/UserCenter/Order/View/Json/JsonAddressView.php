<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use AppMarket\ProductMarket\UserCenter\Order\View\AddressViewTrait;

class JsonAddressView extends JsonView implements IView
{
    use AddressViewTrait;

    public function display() : void
    {
         $list = $this->getAddressList();

         $data = array(
             'list' => $list
         );

         $this->encode($data);
    }
}

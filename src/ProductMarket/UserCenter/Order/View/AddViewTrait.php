<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View;

use Wallet\Coupon\Translator\CouponTranslator;

use AppMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;

use AppMarket\ProductMarket\Workbench\ContractTemplate\Translator\ContractTemplateItemTranslator;

trait AddViewTrait
{
    private $data;

    private $orderData;

    private $couponTranslator;

    private $serviceTranslator;

    private $templateItemTranslator;

    public function __construct($data, $orderData)
    {
        parent::__construct();
        $this->data = $data;
        $this->orderData = $orderData;
        $this->couponTranslator = new CouponTranslator();
        $this->serviceTranslator = new ServiceTranslator();
        $this->templateItemTranslator = new ContractTemplateItemTranslator();
    }

    public function __destruct()
    {
        unset($this->data);
        unset($this->orderData);
        unset($this->couponTranslator);
        unset($this->serviceTranslator);
        unset($this->templateItemTranslator);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getOrderData()
    {
        return $this->orderData;
    }

    public function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    public function getCouponTranslator() : CouponTranslator
    {
        return $this->couponTranslator;
    }

    public function getTemplateItemTranslator() : ContractTemplateItemTranslator
    {
        return $this->templateItemTranslator;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getList()
    {
        $orderData = $this->getOrderData();

        $couponTranslator = $this->getCouponTranslator();
        $serviceTranslator = $this->getServiceTranslator();
        $templateItemTranslator = $this->getTemplateItemTranslator();

        $data = $this->getData();

        $services = !empty($data['service']) ? $data['service'][1] : array();
        $platformCoupons = !empty($data['platformCoupons']) ? $data['platformCoupons'][1] : array();
        $merchantCoupons = !empty($data['merchantCoupons']) ? $data['merchantCoupons'][1] : array();
        $templateItems = !empty($data['templateItem']) ? $data['templateItem'][1] : array();

        $serviceData = array();
        if (!empty($services)) {
            $services = array_values($services);
            $serviceData = $serviceTranslator->objectToArray(
                $services[0],
                array('id', 'title', 'cover', 'price', 'enterprise'=>['id', 'name','logo'], 'snapshots'=>[])
            );
        }

        $platformList = array();
        if (!empty($platformCoupons)) {
            foreach ($platformCoupons as $coupon) {
                $platformList[] = $couponTranslator->objectToArray(
                    $coupon,
                    array('id', 'releaseType', 'status', 'merchantCoupon'=>[])
                );
            }
        }

        $merchantList = array();
        if (!empty($merchantCoupons)) {
            foreach ($merchantCoupons as $coupon) {
                $merchantList[] = $couponTranslator->objectToArray(
                    $coupon,
                    array('id', 'releaseType', 'status', 'merchantCoupon'=>[])
                );
            }
        }

        $templateItemList = array();
        if (!empty($templateItems)) {
            foreach ($templateItems as $templateItem) {
                $templateItemList[] = $templateItemTranslator->objectToArray(
                    $templateItem,
                    array('template'=>['id', 'title'])
                );
            }
        }

        $list = array(
            'merchantCoupon' => $merchantList,
            'platformCoupon' => $platformList,
            'service' => $serviceData,
            'templateItem' => array_shift($templateItemList),
            'order' => $orderData,
            'totalPrice' => $data['number']*$serviceData['price'][$data['sKuIndex']]['value'],
            'realNameAuthenticationStatus' => $data['realNameAuthenticationStatus'],
        );
        return $list;
    }
}

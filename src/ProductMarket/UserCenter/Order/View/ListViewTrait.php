<?php
namespace AppMarket\ProductMarket\UserCenter\Order\View;

use AppMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

use AppMarket\ProductMarket\UserCenter\Contract\Translator\ContractTranslator;

trait ListViewTrait
{
    private $count;

    private $data;

    private $contract;

    private $translator;

    private $contractTranslator;

    public function __construct($count, $data, $contract)
    {
        parent::__construct();
        $this->count = $count;
        $this->data = $data;
        $this->contract = $contract;
        $this->translator = new OrderTranslator();
        $this->contractTranslator = new ContractTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->data);
        unset($this->contract);
        unset($this->translator);
        unset($this->contractTranslator);
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getContract() : array
    {
        return $this->contract;
    }

    public function getTotal()
    {
        return $this->count;
    }

    public function getTranslator() : OrderTranslator
    {
        return $this->translator;
    }

    public function getContractTranslator() : ContractTranslator
    {
        return $this->contractTranslator;
    }

    public function getList()
    {
        $orderList = $this->getData();

        $list = array();
        if (!empty($orderList)) {
            foreach ($orderList as $order) {
                $list[] = $this->getTranslator()->ObjectToArray(
                    $order,
                    array('id','orderno','paymentId','sellerEnterprise'=>['id','name','logo'],'paidAmount','totalPrice',
                    'buyerOrderStatus','status','createTime','timeRecord','orderCommodities'=>['number','skuIndex',
                    'commodity'=>['id','title','cover','price']])
                );
            }
        }

        return $list;
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\Evaluation\Translator;

use Sdk\ProductMarket\Evaluation\Model\EvaluationScore;
use Sdk\ProductMarket\Evaluation\Model\NullEvaluationScore;

use Marmot\Interfaces\ITranslator;

class EvaluationScoreTranslator implements ITranslator
{
    public function arrayToObject(array $evaluationScore, $evaluation = null)
    {
        unset($evaluation);
        unset($evaluationScore);
        return NullEvaluationScore::getInstance();
    }

    public function arrayToObjects(array $evaluationScore) : array
    {
        unset($evaluationScore);
        return array();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function objectToArray($evaluation, array $keys = array())
    {
        if (!$evaluation instanceof EvaluationScore) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'commodityStarReview',
                'enterpriseStarReview'
            );
        }

        $evaluationScore = array();

        if (in_array('id', $keys)) {
            $evaluationScore['id'] = marmot_encode($evaluation->getId());
        }
        if (in_array('commodityStarReview', $keys)) {
            $evaluationScore['commodityStarReview'] = $evaluation->getCommodityStarReview();
        }
        if (in_array('enterpriseStarReview', $keys)) {
            $evaluationScore['enterpriseStarReview'] = $evaluation->getEnterpriseStarReview();
        }

        return $evaluationScore;
    }
}

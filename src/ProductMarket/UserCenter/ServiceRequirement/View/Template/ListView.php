<?php
namespace AppMarket\ProductMarket\UserCenter\ServiceRequirement\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use AppMarket\ProductMarket\UserCenter\ServiceRequirement\View\ListViewTrait;
use AppMarket\ProductMarket\UserCenter\ServiceRequirement\View\StatusTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait, StatusTrait;

    //自定义状态
    const STATUS = array(
        'REVOKED' => -4, //撤销
        'CLOSED' => -6, //关闭
        'DELETED' => -8 //删除
    );

    public function display()
    {
        $list = $this->getList();

        if (!empty($list)) {
            $list = $this->stateTransitionByArray($list);
        }

        $this->getView()->display(
            'ProductMarket/UserCenter/ServiceRequirement/Audit.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['MY_PARTICIPATION'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['SERVICE_REQUIREMENT'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_SERVICE_REQUIREMENT'],
                'list' => $list,
                'total' => $this->getTotal()
            ]
        );
    }
}

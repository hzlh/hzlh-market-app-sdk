<?php
namespace AppMarket\ProductMarket\UserCenter\ServiceRequirement\View\Template;

use Common\View\NavTrait;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\ProductMarket\ServiceRequirement\Model\ServiceRequirement;

use AppMarket\ProductMarket\Workbench\Service\Translator\ServiceTranslator;

use AppMarket\ProductMarket\UserCenter\ServiceRequirement\View\StatusTrait;
use AppMarket\ProductMarket\UserCenter\ServiceRequirement\Translator\ServiceRequirementTranslator;

class DetailView extends TemplateView implements IView
{
    use StatusTrait;

    //自定义状态
    const STATUS = array(
        'REVOKED' => -4, //撤销
        'CLOSED' => -6, //关闭
        'DELETED' => -8 //删除
    );

    private $requirements;

    private $servicesList;

    private $translator;

    private $serviceTranslator;

    public function __construct(ServiceRequirement $requirements, $servicesList)
    {
        parent::__construct();
        $this->requirements = $requirements;
        $this->servicesList = $servicesList;
        $this->translator = new ServiceRequirementTranslator();
        $this->serviceTranslator = new ServiceTranslator();
    }

    public function __destruct()
    {
        unset($this->requirements);
        unset($this->servicesList);
        unset($this->translator);
        unset($this->serviceTranslator);
    }

    protected function getServiceRequirement() : ServiceRequirement
    {
        return $this->requirements;
    }

    protected function getServicesList() : array
    {
        return $this->servicesList;
    }

    protected function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }

    protected function getServiceTranslator() : ServiceTranslator
    {
        return $this->serviceTranslator;
    }

    public function display()
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray($this->getServiceRequirement());
        $data = $this->stateTransitionByOne($data);

        $services = $this->getServicesList();
        $serviceTranslator = $this->getServiceTranslator();

        $servicesArray = array();
        if (!empty($services)) {
            foreach ($services as $service) {
                $servicesArray[] = $serviceTranslator->objectToArray(
                    $service,
                    array(
                        'id',
                        'title',
                        'cover',
                        'minPrice',
                        'pageViews',
                        'volume',
                        'serviceCategory'=>['id','name'],
                        'enterprise'=>['id','logo','name']
                    )
                );
            }
        }

        $this->getView()->display(
            'ProductMarket/UserCenter/ServiceRequirement/Detail.tpl',
            [
                'nav_left_father' => NavTrait::NAV_USER_CENTER['MY_PARTICIPATION'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['SERVICE_REQUIREMENT'],
                'nav_phone' => NavTrait::NAV_PHONE['USER_SERVICE_REQUIREMENT'],
                'data' => $data,
                'serviceList' => $servicesArray
            ]
        );
    }
}

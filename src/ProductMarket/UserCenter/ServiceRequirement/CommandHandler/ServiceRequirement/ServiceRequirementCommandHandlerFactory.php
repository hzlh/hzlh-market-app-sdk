<?php
namespace AppMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'AppMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Core;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\CommandHandler\CloseCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class CloseServiceRequirementCommandHandler extends CloseCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;

    protected function fetchIModifyStatusObject($id) : IModifyStatusAble
    {
        return $this->fetchRequirement($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_CLOSE'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            $this->closeAble->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->closeAble->getNumber()
        );
    }
}

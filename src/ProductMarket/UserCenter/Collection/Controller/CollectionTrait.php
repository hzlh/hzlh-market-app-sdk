<?php
namespace AppMarket\ProductMarket\UserCenter\Collection\Controller;

use Sdk\Collection\Repository\CollectionRepository;
use Marmot\Core;
use Statistical\Controller\StatisticalControllerTrait;

trait CollectionTrait
{
    //use StatisticalControllerTrait;

    private $collectionRepository;

    private function initializationDishonestyInfo()
    {
        $this->collectionRepository = new CollectionRepository();
    }

    public function collectionInfoList(string $category, $primaryId)
    {
        $this->initializationDishonestyInfo();
        $sort = ['-id'];
        $filter = array();

        $filter['memberId'] = Core::$container->get('user')->getId();
        $filter['category'] = $category;
        $filter['status'] = 2;
        $filter['primaryId'] = $primaryId;

        $countFilter['category'] = $category;
        $countFilter['status'] = 2;
        $countFilter['primaryId'] = $primaryId;

        list($count, $collectionInfoList) = $this->collectionRepository
            ->scenario(CollectionRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);

        list($countAll, $collectionInfoListAll) = $this->collectionRepository
            ->scenario(CollectionRepository::PORTAL_LIST_MODEL_UN)
            ->search($countFilter, $sort, PAGE, COMMON_SIZE);

        unset($count);
        unset($collectionInfoListAll);
        return [$collectionInfoList, $countAll];
    }
}

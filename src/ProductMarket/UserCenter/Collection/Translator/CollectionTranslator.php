<?php
namespace AppMarket\ProductMarket\UserCenter\Collection\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\Collection\Model\Collection;
use Sdk\Collection\Model\NullCollection;

use Sdk\Snapshot\Model\Snapshot;

use Snapshot\Translator\SnapshotTranslator;

class CollectionTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $collection = null)
    {
        unset($collection);
        unset($expression);
        return NullCollection::getInstance();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function getSnapshotsTranslator() : SnapshotTranslator
    {
        return new SnapshotTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($collection, array $keys = array())
    {
        if (!$collection instanceof Collection) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'category',
                'primaryId',
                'primaryObject',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = $collection->getId();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $collection->getCategory();
        }
        if (in_array('primaryId', $keys)) {
            $expression['primaryId'] = $collection->getPrimaryId();
        }
        if (in_array('primaryObject', $keys)) {
            $expression['primaryObject'] = $collection->getPrimaryObject();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $collection->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $collection->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $collection->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $collection->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $collection->getUpdateTime());
        }

        return $expression;
    }
}

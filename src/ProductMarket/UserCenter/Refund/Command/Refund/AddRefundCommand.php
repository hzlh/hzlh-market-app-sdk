<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\Command\Refund;

use Marmot\Interfaces\ICommand;

class AddRefundCommand implements ICommand
{
    public $refundReason;

    public $serviceOrderId;

    public $memberId;

    public function __construct(
        string $refundReason,
        int $serviceOrderId,
        int $memberId,
        int $id = 0
    ) {
        $this->refundReason = $refundReason;
        $this->serviceOrderId = $serviceOrderId;
        $this->memberId = $memberId;
        $this->id = $id;
    }
}

<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\CommandHandler\Refund;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use AppMarket\ProductMarket\UserCenter\Refund\Command\Refund\AddRefundCommand;


use Sdk\ProductMarket\Member\Repository\MemberRepository;

use Sdk\ProductMarket\Enterprise\Repository\EnterpriseRepository;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;

use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

class AddRefundCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    private $serviceOrder;

    private $refundOrder;

    private $orderRepository;

    private $enterpriseRepository;

    private $memberRepository;

    public function __construct()
    {
        $this->serviceOrder = new ServiceOrder();
        $this->refundOrder = new RefundOrder();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->orderRepository = new ServiceOrderRepository();
        $this->memberRepository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->serviceOrder);
        unset($this->refundOrder);
        unset($this->enterpriseRepository);
        unset($this->orderRepository);
        unset($this->memberRepository);
    }

    protected function getRefundOrder() : RefundOrder
    {
        return $this->refundOrder;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->orderRepository;
    }

    protected function fetchOrder(int $id) : ServiceOrder
    {
        return $this->getServiceOrderRepository()->fetchOne($id);
    }

    protected function fetchMember($id)
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function fetchEnterprise($id)
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction($command)
    {
        if (!($command instanceof AddRefundCommand)) {
            throw new \InvalidArgumentException;
        }

        $refundOrder = $this->getRefundOrder();

        $member =  $this->fetchMember($command->memberId);
        $refundOrder->setMember($member);

        $refundOrder->setRefundReason($command->refundReason);

        $serviceOrder = $this->fetchOrder($command->serviceOrderId);
        $refundOrder->setOrder($serviceOrder);

        $enterpriseId = $serviceOrder->getSellerEnterprise()->getId();
        $enterprise = $this->fetchEnterprise($enterpriseId);
        $refundOrder->setEnterprise($enterprise);

        if ($refundOrder->add()) {
            $command->id = $refundOrder->getId();
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ORDER_REFUND'],
            ILogAble::CATEGORY['REFUND_ORDER'],
            $this->getRefundOrder()->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('user'),
            $this->getRefundOrder()->getRefundReason()
        );
    }
}

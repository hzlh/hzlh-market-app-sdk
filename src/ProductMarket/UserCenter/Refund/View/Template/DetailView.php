<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\View\Template;

use Common\View\NavTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;

use AppMarket\ProductMarket\UserCenter\Refund\Translator\RefundTranslator;

class DetailView extends TemplateView implements IView
{
    private $refundOrder;

    private $translator;

    public function __construct(RefundOrder $refundOrder)
    {
        parent::__construct();
        $this->refundOrder = $refundOrder;
        $this->translator = new RefundTranslator();
    }

    public function __destruct()
    {
        unset($this->refund);
        unset($this->translator);
    }

    protected function getRefundOrder() : RefundOrder
    {
        return $this->refundOrder;
    }

    protected function getTranslator() : RefundTranslator
    {
        return $this->translator;
    }

    public function getData()
    {
        $translator = $this->getTranslator();

        $data = $translator->objectToArray(
            $this->getRefundOrder()
        );

        return $data;
    }

    public function display()
    {
        $data = $this->getData();

        $this->getView()->display(
            'ProductMarket/UserCenter/Refund/Detail.tpl',
            [
                'data' => $data,
                'nav_left_father' => NavTrait::NAV_USER_CENTER['TRANSACTION_MANAGEMENT'],
                'nav_left' => NavTrait::NAV_USER_CENTER_SECOND['REFUND']
            ]
        );
    }
}

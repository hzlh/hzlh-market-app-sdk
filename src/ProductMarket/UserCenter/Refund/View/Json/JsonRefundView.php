<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use AppMarket\ProductMarket\UserCenter\Order\Translator\OrderTranslator;

class JsonRefundView extends JsonView implements IView
{
    private $data;

    public function __construct($data)
    {
        parent::__construct();
        $this->data = $data;
    }

    protected function getData()
    {
        return $this->data;
    }

    public function display() : void
    {
        $data = array(
            'refundId' => $this->getData(),
        );

        $this->encode($data);
    }
}

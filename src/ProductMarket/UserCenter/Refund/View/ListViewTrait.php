<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\View;

use AppMarket\ProductMarket\UserCenter\Refund\Translator\RefundTranslator;

trait ListViewTrait
{
    private $refundList;

    private $count;

    private $translator;

    public function __construct(
        array $refundOrderList,
        int $count
    ) {
        parent::__construct();
        $this->count = $count;
        $this->refundOrderList = $refundOrderList;
        $this->translator = new RefundTranslator();
    }

    public function __destruct()
    {
        unset($this->count);
        unset($this->refundList);
        unset($this->translator);
    }

    protected function getRefundOrderList() : array
    {
        return $this->refundOrderList;
    }

    protected function getRefundTotal()
    {
        return $this->count;
    }

    protected function getTranslator() : RefundTranslator
    {
        return $this->translator;
    }

    protected function getRefundList()
    {
        $translator = $this->getTranslator();

        $list = array();
        foreach ($this->getRefundOrderList() as $refund) {
            $list[] = $translator->objectToArray(
                $refund,
                array(
                    'id',
                    'refundReason',
                    'refuseReason',
                    'voucher',
                    'status',
                    'createTime',
                    'statusTime',
                    'order'=>[]
                )
            );
        }
        return $list;
    }
}

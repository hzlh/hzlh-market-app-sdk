<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\GlobalCheckTrait;

use Sdk\ProductMarket\Order\RefundOrder\Adapter\IRefundOrderAdapter;
use Sdk\ProductMarket\Order\RefundOrder\Model\RefundOrder;
use Sdk\ProductMarket\Order\RefundOrder\Repository\RefundOrderRepository;
use AppMarket\ProductMarket\UserCenter\Refund\View\Json\JsonListView;
use AppMarket\ProductMarket\UserCenter\Refund\View\Template\ListView;
use AppMarket\ProductMarket\UserCenter\Refund\View\Template\DetailView;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new RefundOrderRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IRefundOrderAdapter
    {
        return $this->repository;
    }

    public function filterAction()
    {
        if (!$this->globalCheck()) {
            return false;
        }

        list($page, $size) = $this->getPageAndSize(FORM_SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        $refundOrderList = array();
        list($count, $refundOrderList) = $this->getRepository()
            ->scenario(RefundOrderRepository::PORTAL_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new JsonListView($refundOrderList, $count));
            return true;
        }

        $this->render(new ListView($refundOrderList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        $orderNumber = $request->get('orderNumber', '');
        $status = $request->get('status', '');
        $goodsName = $request->get('goodsName', '');
        $createTime = $request->get('createTime', '');

        $sort = ['-updateTime'];
        $filter = array();

        if ($orderNumber !== '') {
            $filter['orderNumber'] = $orderNumber;
        }
        if ($status !== '' && in_array($status, RefundOrder::REFUND_STATUS)) {
            $filter['status'] = $status;
        }
        if ($goodsName !== '') {
            $filter['goodsName'] = $goodsName;
        }
        if (!empty($createTime)) {
            $filter['createTime'] = strtotime($createTime);
        }

        $filter['member'] = Core::$container->get('user')->getId();

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        if (!$this->globalCheck()) {
            return false;
        }

        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $refundOrder = $this->getRepository()
            ->scenario(RefundOrderRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id)
        ;
        if ($refundOrder instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DetailView($refundOrder));
        return true;
    }
}

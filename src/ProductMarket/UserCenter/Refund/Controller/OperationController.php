<?php
namespace AppMarket\ProductMarket\UserCenter\Refund\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;

use AppMarket\ProductMarket\UserCenter\Refund\CommandHandler\Refund\RefundCommandHandlerFactory;

use AppMarket\ProductMarket\UserCenter\Refund\Command\Refund\AddRefundCommand;

use AppMarket\ProductMarket\UserCenter\Refund\View\Json\JsonRefundView;

/**
 * @SuppressWarnings(PHPMD)
 */
class OperationController extends Controller
{
    use WebTrait, GlobalCheckTrait, OperationValidateTrait;


    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new RefundCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * @return bool
     * @param [POST]
     * @method /userCenter/orders/{id}/refunds
     *
     * 申请退款
     */
    public function refund($id) : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $request = $this->getRequest();

        $orderId = marmot_decode($id);
        if (empty($orderId)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }

        $refundReason = $request->post('refundReason', '');
        $memberId = Core::$container->get('user')->getId();

        $refundReason = htmlspecialchars_decode($this->formatString($refundReason), ENT_QUOTES);

        if ($this->validateOperationScenario($refundReason)) {
            $command = new AddRefundCommand(
                $refundReason,
                $orderId,
                $memberId
            );

            if ($this->getCommandBus()->send($command)) {
                $data = marmot_encode($command->id);
                
                $this->render(new JsonRefundView($data));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function formatString($str)
    {
      //ℑ为占位符，替换掉&lt;br&gt;，用来做字符串长度的验证
        $str = str_replace("&lt;br&gt;", "ℑ", $str);
        $str = str_replace("\'", "'", $str);

        return $str;
    }
}

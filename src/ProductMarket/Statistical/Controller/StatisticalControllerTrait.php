<?php
namespace AppMarket\ProductMarket\Statistical\Controller;

use Sdk\Statistical\Repository\StatisticalRepository;
use Sdk\Statistical\Adapter\StatisticalAdapterFactory;

use Qxy\Contract\Statistical\Repository\StatisticalRepository as ContractStatisticalRepository;
use Qxy\Contract\Statistical\Adapter\StatisticalAdapterFactory as ContractStatisticalAdapterFactory;

trait StatisticalControllerTrait
{
    protected function getRepository(string $type) : StatisticalRepository
    {
        $adapterFactory = new StatisticalAdapterFactory();
        $adapter = $adapterFactory->getAdapter($type);

        return new StatisticalRepository(new $adapter);
    }

    public function analyse(string $type, array $filter = [])
    {
        $repository = $this->getRepository($type);

        $statistical = $repository->analyse($filter);

        return $statistical;
    }

    protected function getContractRepository(string $type) : ContractStatisticalRepository
    {
        $adapterFactory = new ContractStatisticalAdapterFactory();
        $adapter = $adapterFactory->getAdapter($type);

        return new ContractStatisticalRepository(new $adapter);
    }

    public function contractAnalyse(string $type, array $filter = [])
    {
        $repository = $this->getContractRepository($type);

        $statistical = $repository->analyse($filter);

        return $statistical;
    }
}

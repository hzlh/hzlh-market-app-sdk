<?php
namespace AppMarket\ProductMarket\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use Qxy\Contract\Statistical\Model\Statistical;
use Qxy\Contract\Statistical\Model\NullStatistical;

/**
  * @SuppressWarnings(PHPMD.CyclomaticComplexity)
  * @SuppressWarnings(PHPMD.NPathComplexity)
  */
class StaticsContractUsageCountTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return NullStatistical::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();

        if (in_array('result', $keys)) {
            $expression = $statistical->getResult();
        }

        return $expression;
    }
}

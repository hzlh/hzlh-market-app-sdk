<?php
namespace AppMarket\ProductMarket\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

class StaticsTranslatorFactory
{
    const MAPS = array(
        'staticsActiveEnterprise'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsActiveEnterpriseTranslator',
        'staticsServiceAuthenticationCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsServiceAuthenticationCountTranslator',
        'staticsServiceRequirementCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsServiceRequirementCountTranslator',
        'staticsServiceCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsServiceCountTranslator',
        'staticsMemberCouponCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsMemberCouponCountTranslator',
        'staticsEnterpriseOrderVolume'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsEnterpriseOrderVolumeTranslator',
        'staticsLoanProductCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsProductCountTranslator',
        'staticsFinanceCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsFinanceCountTranslator',
        'staticsEnterpriseLoanProductInfoCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsEnterpriseLoanProductInfoCountTranslator',
        'staticsBusinessNotice'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsBusinessNoticeTranslator',
        'staticsCommodityScore'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsCommodityScoreTranslator',
        'staticsEnterpriseScore'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsEnterpriseScoreTranslator',
        'staticsCollectionByMember'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsCollectionByMemberTranslator',
        'staticsCollectionByCategoryOrDetail'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsCollectionByCategoryOrDetailTranslator',
        'staticsCommodityByEnterprise'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsCommodityByEnterpriseTranslator',
        'staticsContractPerformanceCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsContractPerformanceCountTranslator',
        'staticsContractUsageCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsContractUsageCountTranslator',
        'staticsEvaluateGradeCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsEvaluateGradeCountTranslator',
        'staticsEnterpriseContractCount'=>
        'AppMarket\ProductMarket\Statistical\Translator\StaticsEnterpriseContractCountTranslator',
    );

    public function getTranslator(string $type) : ITranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : false;
    }
}

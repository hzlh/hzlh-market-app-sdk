<?php
namespace AppMarket\ProductMarket\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\Model\Statistical;
use Sdk\Model\NullStatistical;

class StaticsCommodityByEnterpriseTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return NullStatistical::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();

        if (in_array('result', $keys)) {
            $expression = $statistical->getResult();
        }

        return $expression;
    }
}

<?php
namespace AppMarket\ProductMarket\Statistical\View\Json;

use Statistical\Translator\StaticsTranslatorFactory;
use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\ProductMarket\Statistical\Model\Statistical;

class StatisticalView extends JsonView implements IView
{
    private $statistical;

    private $type;

    public function __construct(Statistical $statistical, string $type)
    {
        $this->statistical = $statistical;
        $this->type = $type;
        parent::__construct();
    }

    protected function getStatistical() : Statistical
    {
        return $this->statistical;
    }

    protected function getType() : string
    {
        return $this->type;
    }

    protected function getTranslator(string $type)
    {
        $translatorFactory = new StaticsTranslatorFactory();

        $translator = $translatorFactory->getTranslator($type);

        return new $translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator($this->getType());

        $data = $translator->objectToArray(
            $this->getStatistical()
        );
        
        $this->encode($data);
    }
}

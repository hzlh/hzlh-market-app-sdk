<?php
namespace AppMarket\ProductMarket\Statistical\View;

use Statistical\Translator\StaticsTranslatorFactory;

use Sdk\Statistical\Model\Statistical;

trait StatisticalViewTrait
{
    protected function getStatisticalTranslator(string $type)
    {
        $translatorFactory = new StaticsTranslatorFactory();
    
        $translator = $translatorFactory->getTranslator($type);
      
        return new $translator;
    }

    public function statisticalArray(string $type, Statistical $statistical)
    {
        $data = array();
    
        $translator = $this->getStatisticalTranslator($type);

        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }
}
